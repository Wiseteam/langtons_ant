import turtle

ant = turtle.Turtle()
ant.color('white')
ant.shape('square')
maps = {}

def coordinates(ant): 
    return (int(ant.xcor()),int(ant.ycor()))
 
def ant_right():
    ant.stamp() 
    ant.right(90) 
    ant.forward(20)

def ant_left():
    ant.stamp()
    ant.left(90)
    ant.forward(20)

def ant_cod_colour():
    
    if coordinates(ant) not in maps or maps[coordinates(ant)] == 'white':                                                                     
        return('black')
    else:
        return('white')

def ant_move(): 
   
    window = turtle.Screen() 
    window.bgcolor('white') 
    window.screensize(2500,2500)
     
    
    while True:

        if coordinates(ant) not in maps:
            state = 0
        else:
            state = 1
            
        if state == 0 or maps[coordinates(ant)] == 'white': 
            ant.fillcolor("black")  
              
            ant_right()

        elif state = 1: 
            ant.fillcolor("white")  
              
            ant_left()
        
if coordinates(ant) in maps:
    state = 1
else:
    state = 0
    

ant_move() 
